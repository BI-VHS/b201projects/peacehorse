# Toby (pracovní název)

## Trailer

[odkaz na Youtube](https://www.youtube.com/watch?v=v3EH031kMpc&t)

## Základní popis

Jedná se o 2.5D plošinovku využívající převážně low poly grafiku. Hra je rozdělená na několik levelů, které mají spíše lineární průchod zaměřený hodně na příběh a atmosféru.

## Příběh 

Hlavní hrdina není lidské ani nám známé rasy. Žije kdesi v neznámém vesmíru na jedné neznámé planetě v malém městečku. Technologie této planety umožnila vytvořit velmi inteligentních
robotů, kteří jsou běžnou součástí tamní společnosti. Jednoho dne se však život na této planetě nečekaně změní, když po probuzení nemůže náš hrdina najít žádné živé obyvatele, místo
nich zbyli jen roboti. A tak se sám vydává do světa zjistit pravdu. 

## Scény

- Městečko
- Jeskyně
- Temný les
- Opuštěný důl
- Továrna

## Dokumentace

- [První část dokumentace: statický svět](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/peacehorse/blob/master/dokumentace1.adoc)

- [Druhá část dokumentace: dynamický svět](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/peacehorse/blob/master/dokumentace2.adoc)

- [Třetí část dokumentace: komplexní svět](https://gitlab.fit.cvut.cz/BI-VHS/b201projects/peacehorse/blob/master/dokumentace3.adoc)