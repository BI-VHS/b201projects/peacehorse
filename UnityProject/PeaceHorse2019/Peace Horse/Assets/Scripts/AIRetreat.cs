﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRetreat : MonoBehaviour {

    public float speed; // his speed
    public float stoppingDistance; // where enemy stops
    public float retreatDistance; // when enemy runs away
    public float sightDistance;

    public Transform[] moveSpots; // destinatios of retreating robot
    public Transform player;

    private int spot; // pick random spot from spot array
    private bool inSight; // keep track if enemy is in sight of the player

    private float waitTime;
    public float startWaitTime;

    void Start(){
        waitTime = startWaitTime;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        inSight = false;
        spot = 0;
    }

    void Update(){
        // update only if enemy is in sight
        if (Vector2.Distance(transform.position, player.position) <= sightDistance && spot == 0){
            inSight = true;
        }

        if(inSight){
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[spot].position, speed * Time.deltaTime);
            if(Vector2.Distance(transform.position, moveSpots[spot].position) < 0.2f){
                if(waitTime <= 0) {
                    spot++;
                    // ai reached its final destination
                    if (spot > moveSpots.Length)
                        inSight = false;
                    waitTime = startWaitTime;
                } else {
                    waitTime -= Time.deltaTime;
                }
            }
        }
    }
}
