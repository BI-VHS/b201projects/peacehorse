﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObjectScript : MonoBehaviour
{
	public float fallSpeed = 8.0f;

	private Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

	void OnCollisionEnter(Collision collision){
		Debug.Log("collision");
		transform.position = startPos;
	}
}
