﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public void QuitButtonFunction()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }
    public void SelectScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
}
