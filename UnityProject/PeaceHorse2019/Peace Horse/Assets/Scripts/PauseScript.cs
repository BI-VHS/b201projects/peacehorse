﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScript : MonoBehaviour
{
    public static bool isPaused = false;
    [SerializeField]
    private GameObject PauseMenu;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                this.Resume();
            }
            else
            {
                this.Pause();
            }
        }
    }

    private void Resume()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

    private void Pause()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void QuitButtonFunction()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }

    public void MainMenuButtonFunction()
    {
        this.Resume();
        SceneManager.LoadScene("Menu");
    }

    public void RestartLevelButtonFunction()
    {
        this.Resume();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ResumeButtonFunction()
    {
        if (isPaused)
        {
            this.Resume();
        }
    }
}
