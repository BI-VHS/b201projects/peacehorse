﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float movementSpeed = 1;
    public float jumpForce = 1;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public int maxJumps = 1;

    private Rigidbody playerRigidbody;
    private int jumpsRemaining;
    private int extraJumps = 0;
    private Collider[] groundCollisions;
    private float groundCheckRadius = 0.1f;
    private bool jump = false;
    private bool grounded = false;


    // Start is called before the first frame update
    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        jumpsRemaining = maxJumps;
    }

    // Update is called once per frame
    private void Update()
    {
        if(Input.GetKeyDown("space") || Input.GetKeyDown("w"))
        {
            jump = true;
        }
    }

    private void FixedUpdate()
    {
        if (jump && (jumpsRemaining + extraJumps > 0))
        {
            playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, Mathf.Min(playerRigidbody.velocity.y, 0), playerRigidbody.velocity.z);
            if((jumpsRemaining > 0))
            {
                --jumpsRemaining;
                playerRigidbody.AddForce(new Vector3(0, jumpForce * 4, 0), ForceMode.Impulse);
            }
            else if (extraJumps > 0)
            {
                --extraJumps;
                playerRigidbody.AddForce(new Vector3(0, jumpForce * 4, 0), ForceMode.Impulse);
            }
            
        }
        else
        {
            groundCollisions = Physics.OverlapSphere(groundCheck.position, groundCheckRadius, groundLayer);
            if (groundCollisions.Length > 0)
            {
                grounded = true;
                jumpsRemaining = maxJumps;
            }
            else
            {
                grounded = false;
            }
        }
        if(!grounded && jumpsRemaining == maxJumps)
        {
            --jumpsRemaining;
        }
        jump = false;


        var movement = Input.GetAxis("Horizontal");
        playerRigidbody.velocity = new Vector3(movementSpeed * movement, playerRigidbody.velocity.y,0);

    }

    public void setMaxJumps(int newValue)
    {
        maxJumps = newValue;
    }

    public void addJump(int toAdd)
    {
        extraJumps = Mathf.Max(extraJumps, toAdd);
    }
}
