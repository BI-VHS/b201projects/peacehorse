﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private Vector3 activeCheckpointPosition;
    private Rigidbody playerRigidbody;
    private int ValuablesCollected = 0;
    private int ReadablesCollected = 0;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        activeCheckpointPosition = gameObject.transform.position;
    }

    public void respawnAtActiveCheckpoint()
    {
        Debug.Log("RIP");
        playerRigidbody.velocity = new Vector3(0, 0, 0);
        gameObject.transform.position = activeCheckpointPosition;
    }
    public void updateActiveCheckpoint(Vector3 newCheckpointPosition)
    {
        Debug.Log("active checkpoint updated");
        activeCheckpointPosition = newCheckpointPosition;
    }

    public void ValuablePickUp()
    {
        ++ValuablesCollected;
        Debug.Log("total valuables collected: " + ValuablesCollected);
    }
    public void ReadablePickUp()
    {
        ++ReadablesCollected;
        Debug.Log("total readables collected: " + ReadablesCollected);
    }
}
