﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] private float ParallaxEffectMultiplierX = 0.5f;
    [SerializeField] private float ParallaxEffectMultiplierY = 0.2f;

    private Transform cameraTransform;
    private Vector3 previousCameraPosition;
    // Start is called before the first frame update
    private void Start()
    {
        cameraTransform = Camera.main.transform;
        previousCameraPosition = cameraTransform.position;
    }

    private void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - previousCameraPosition;
        transform.position += new Vector3(deltaMovement.x * ParallaxEffectMultiplierX,
            deltaMovement.y * ParallaxEffectMultiplierY, 0);
        previousCameraPosition = cameraTransform.position;
    }
}
