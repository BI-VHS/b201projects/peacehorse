﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrigger : MonoBehaviour
{

    public GameObject cameraController;
    public int camId = 1;
    private Animator cameraAnimator;
    // Start is called before the first frame update
    private void Start()
    {
        if (cameraController != null)
        {
            cameraAnimator = cameraController.GetComponent<Animator>();
        }
        else
        {
            Debug.LogWarning("Aby camera trigger fungoval, musí mít přiřazen cameraController");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (cameraAnimator != null)
        {
            if (other.gameObject.CompareTag("Player"))
            {
                cameraAnimator.SetInteger("camId", camId);
            }
        }
    }
}
