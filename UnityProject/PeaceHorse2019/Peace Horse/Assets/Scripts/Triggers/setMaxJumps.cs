﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class setMaxJumps : MonoBehaviour
{
    public int setMaxJumpsTo = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerMovement>().setMaxJumps(setMaxJumpsTo);
            gameObject.SetActive(false);
        }
    }
}
