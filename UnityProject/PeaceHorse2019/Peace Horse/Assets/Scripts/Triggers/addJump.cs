﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addJump : MonoBehaviour
{
    public int numberOfJumpsToAdd = 1;
    public bool respawn = true;
    public float respawnTime = 5f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<PlayerMovement>().addJump(numberOfJumpsToAdd);
            if (respawn)
            {
                Invoke("reactivate", respawnTime);
            }
            gameObject.SetActive(false);
        }
    }
    private void reactivate()
    {
        gameObject.SetActive(true);
    }
}