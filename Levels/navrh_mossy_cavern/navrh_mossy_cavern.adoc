:toc:
# Návrh levelu - Organická jeskyně

image::images/mossycav.jpg[,500]

Zdroj: https://maaot.itch.io/mossy-cavern

## Prvky

### Mechové přichytávací povrchy

Zpomalí postavu - jiné vlastnosti pohybu, skákání.

### Jezera plná vody

Voda je otrávená od mechů a lišejníků, otráví hráče - debuffy.

### Omezení času na level

V jeskyních je vlhko robot tudíž rychle koroduje. Musí hledat ukryté části jeskyně, kam se mechy nerozšířily a tam se opravovat.

## Ostatní

### Mluvící jeskyně

Jediná opravdu žijící entita na planetě. Stále ne úplně vyvinutá i když je to po tisících let. Potřebná interakce s ní - domluva?




