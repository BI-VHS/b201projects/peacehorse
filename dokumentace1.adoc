= Statický svět

* Zadání: 3 - 4 scény
	** 3+ druhů statických objektů (krajina), 5 unikátů
	** 3+ druhů dynamických objektů (zvěř, lidi), 3 unikáty
	** 1+ druhů obydlí, 2 unikáty

* Scény
    ** Úvodní vesnice
    ** Opuštěné doly
    ** Temný les
    ** Jeskyně

# Jeskyně

* statické objekty: kameny, svítící krystaly v kamenech, stalakti/mi/náty, kostra draka, kamenné oblouky, ostré kusy ledu
* jako dynamický objekt bude sloužit samotná jeskyně, která bude s hráčem komunikovat, bude potřeba ji dostat na svoji stranu, aby hráče nechala projít
* obydlí: dračí skrýš

image::Levels/navrh_mossy_cavern/images/scene.png[,500]
image::Levels/navrh_mossy_cavern/images/dragon.png[,500]

# Opuštěný důl

* statické objekty: kameny, krystaly, jeskynní tvary, bedny, důlní vozík, koleje, kosti
* dynamické objekty: jeskynní šnek

image::Levels/navrh_cavern/images/static_objects.png[,500]

# Temný les

* statické objekty: platformy, stromy, klády, větve, kořeny, lišejník, skály, kameny, pařezy, keře, tráva, lucerny, louče, truhly, most, vodopád, toxické jezero, krystaly, ostny, pavučiny
* dynamické objekty: velký a malý pavouk, 4 moudré stromy (ve scéně zatím jen jeden), světlušky
* obydlí: pavoučí pavučina

image::Levels/navrh_temny_les/images/waterfall.png[,500]
image::Levels/navrh_temny_les/images/wise_tree.png[,500]
image::Levels/navrh_temny_les/images/spider.png[,500]